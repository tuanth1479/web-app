<?php
/**
 *
 * @SWG\Tag(
 *   name="Users",
 *   description="Operations about User",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 *
 * @SWG\Tag(
 *   name="Admin",
 *   description="Operations about Admin",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 *
 * @SWG\Tag(
 *   name="Authentication",
 *   description="Description about Authentication",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 *
 * @SWG\Tag(
 *   name="Products",
 *   description="Description about Products",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 *
 * @SWG\Tag(
 *   name="Categories",
 *   description="Description about Categories",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 *
 * @SWG\Tag(
 *   name="Stocks",
 *   description="Description about Categories",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 */
