<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class CategoryController extends BaseApiController
{
    //get category with id:
    //will return all products in that category + info of that category
    //format: { info:
    //          products:
    //        }

    public function getInforOfCategory(Request $request)
    {
        /**
         * @SWG\Get(
         *      path="/category/{category_id}",
         *      tags={"Categories"},
         *      description="Get infor of one cat through category_id",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *      @SWG\Parameter(
         *         description="ID category to show",
         *         in="path",
         *         name="category_id",
         *         required=true,
         *         type="string",
         *      ),
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */

        try {
            // $category_products = Product::select('*')
            // ->where('category_id', 'like', $request->category_id)
            // ->paginate(10)
            // ->withPath("/category")
            // ;

            $category_info = Category::select("*")
                ->where('category_id', 'like', $request->id)
                ->first();

            $result = [
                'category_info' => $category_info,
                // 'products' => $category_products,
            ];

            return $this->responseSuccess($result);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    //function will return a list of all categories which database have
    //to show list category for UI
    public function getAllCategory(Request $request)
    {
        /**
         * @SWG\Get(
         *      path="/category/all",
         *      tags={"Categories"},
         *      description="Get list categories",
         *      summary="Get list categories",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */

        try {
            $result = Category::all();
            return $this->responseSuccess($result);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function createCategory(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/category/create",
         *      operationId="createCategory",
         *      tags={"Categories"},
         *      summary="Create category",
         *      description="Create category",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *      @SWG\Parameter(
         *          name="category_id",
         *          description="Category's id",
         *          in="formData",
         *          type="string",
         *          required=true,
         *      ),
         *      @SWG\Parameter(
         *          name="name",
         *          description="Category's name",
         *          in="formData",
         *          type="string",
         *          required=true,
         *      ),
         *      @SWG\Parameter(
         *          name="description",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="image",
         *          description="Image of Category",
         *          in="formData",
         *          type="file",
         *      ),
         *      @SWG\Response(response=200, description="Success"),
         *      @SWG\Response(response=400, description="Invalid request params"),
         *      @SWG\Response(response=401, description="Request is not authenticated"),
         *      @SWG\Response(response=404, description="Not Found"),
         *     )
         *
         */
        try {
            //validate
            $validator = Category::validate($request->all(), "Rule_Create_Category");
            if ($validator) {
                return $this->responseErrorValidator($validator, 422);
            }
            //check constraint
            $check = Category::checkConstraint($request->name);
            if ($check) {
                return $this->responseErrorCustom("Category's infor is illegal", 422);
            }
            //save image
            $link = $this->saveImage($request, 'category', NULL);

            //save infor to database
            $category = new Category;
            $category->category_id = $request->category_id;
            $category->name = $request->name;
            $category->description = $request->description;
            $category->image_link = $link;
            $category->save();

            return $this->responseSuccess($category);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 99999, 500);
        }
    }

    public function updateCategory(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/category/update/{cat_id}",
         *      operationId="updateCategory",
         *      tags={"Categories"},
         *      summary="Update 1 category",
         *      description="Update 1 category",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *      @SWG\Parameter(
         *         description="ID category to update",
         *         in="path",
         *         name="cat_id",
         *         required=true,
         *         type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="name",
         *          description="Category's name",
         *          in="formData",
         *          type="string",
         *          required=true,
         *      ),
         *      @SWG\Parameter(
         *          name="description",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="image",
         *          description="Image of Category",
         *          in="formData",
         *          type="file",
         *      ),
         *      @SWG\Response(response=200, description="Success"),
         *      @SWG\Response(response=400, description="Invalid request params"),
         *      @SWG\Response(response=401, description="Request is not authenticated"),
         *      @SWG\Response(response=404, description="Not Found"),
         *     )
         *
         */
        try {
            //validate
            $validator = Category::validate($request->all(), "Rule_Update_Category");
            if ($validator) {
                return $this->responseErrorValidator($validator, 422);
            }

            //take category
            $category = Category::where(['category_id' => $request->cat_id])->first();
            if (!$category) {
                return $this->responseErrorCustom('not_found', 404);
            }

            //check valid name to update
            $check = Category::where(['name' => $request->name])->first();
            if ($check) {
                if ($check->id != $request->cat_id) {
                    return $this->responseErrorCustom('invalid_cat_name', 422);
                }
            }

            $link = $this->saveImage($request, 'category', $category->image_link);

            //save to database
            Category::where(['category_id' => $request->cat_id])->update([
                'name' => $request->name,
                'description' => $request->description,
                'image_link' => $link
            ]);
            $catInfor = Category::where(['category_id' => $request->cat_id])->first();

            // $category->name = $request->name;
            // $category->description = $request->description;
            // $category->image_link = $link;
            // $category->save();

            return $this->responseSuccess($catInfor);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 99999, 500);
        }
    }
}
