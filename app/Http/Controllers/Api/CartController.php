<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartDetail;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends BaseApiController
{

    public function viewCart(Request $request)
    {
        /**
         * @SWG\Get(
         *      path="/cart/view/{cart_id}",
         *      tags={"Carts"},
         *      description="Carts",
         *      summary="Carts",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *       @SWG\Parameter(
         *         description="ID cart",
         *         in="path",
         *         name="cart_id",
         *         required=true,
         *         type="integer",
         *         format="int64"
         *     ),
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $user_id = $request->user->id;
            $cart_id = $request->cart_id;
            $checkUserCart = Cart::userCartAvailable($user_id);

            //if user does not have any cart => create new cart
            if (!$checkUserCart) {
                return $this->responseErrorCustom("cart not found", 404);
            }
            //update cart information
            Cart::updateCart($cart_id);

            $cartInfor = [
                "empty"
            ];

            $userCart = Cart::getUserCart($user_id);
            $cartInfor = CartDetail::getCartDetail($cart_id);

            $returnString = [
                'cartInfor' => $cartInfor,
                'cart' => $userCart
            ];

            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function addToCart(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/cart/add",
         *      tags={"Carts"},
         *      description="Carts",
         *      summary="Carts",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="import",
         *          description="stock import",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="product_id",
         *                  type="string",
         *              ),
         *
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $user_id = $request->user->id;
            $product_id = $request->product_id;
            // $incrementNumberOfQuantity = $request->quantity;

            $product = Product::productAvailable($product_id);
            if (!$product) {
                return $this->responseErrorCustom("product_notfound", 404);
            }

            $productPrice = $product->product_price;

            $checkUserCart = Cart::userCartAvailable($user_id);

            //if user does not have any cart => create new cart
            if (!$checkUserCart) {
                $check = Cart::createUserCart($user_id);
                if ($check == -1) {
                    return $this->responseErrorCustom("can not create cart", 500);
                }
            }

            $userCart = Cart::userCartAvailable($user_id);
            $cart_id = $userCart->cart_id;

            $check = CartDetail::checkIsProductAvailableInCartDetail($cart_id, $product_id);
            // return $this->responseSuccess($check);
            if ($check == -1) {
                $rs = CartDetail::createProductInforToCartDetail($cart_id, $product_id, $productPrice);
                if ($rs == -1) {
                    return $this->responseErrorCustom("can not create product infor to cart", 500);
                }
            } elseif ($check == 1) {
                $rs = CartDetail::updateProductInforToCartDetail($cart_id, $product_id, $productPrice, 1);
                if ($rs == -1) {
                    return $this->responseErrorCustom("can not update product infor to cart", 500);
                }
            }

            //update cart information
            Cart::updateCart($cart_id);

            $cartInfor = [
                "empty"
            ];

            $userCart = Cart::getUserCart($user_id);
            $cartInfor = CartDetail::getCartDetail($cart_id);

            $returnString = [
                'cartInfor' => $cartInfor,
                'cart' => $userCart
            ];

            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function subtractFromCart(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/cart/subtract",
         *      tags={"Carts"},
         *      description="Carts",
         *      summary="Carts",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="import",
         *          description="subtract",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="product_id",
         *                  type="string",
         *              ),
         *
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $user_id = $request->user->id;
            $product_id = $request->product_id;
            // $incrementNumberOfQuantity = $request->quantity;

            $product = Product::productAvailable($product_id);
            if (!$product) {
                return $this->responseErrorCustom("product_notfound", 404);
            }

            $productPrice = $product->product_price;

            $checkUserCart = Cart::userCartAvailable($user_id);

            //if user does not have any cart => create new cart
            if (!$checkUserCart) {
                $check = Cart::createUserCart($user_id);
                if ($check == -1) {
                    return $this->responseErrorCustom("can not create cart", 500);
                }
            }

            $userCart = Cart::userCartAvailable($user_id);
            $cart_id = $userCart->cart_id;

            $check = CartDetail::checkIsProductAvailableInCartDetail($cart_id, $product_id);
            // return $this->responseSuccess($check);
            if ($check == -1) {
                return $this->responseErrorCustom("can not find product in cart", 500);
            } elseif ($check == 1) {
                $rs = CartDetail::updateProductInforToCartDetail($cart_id, $product_id, $productPrice, -1);
                if ($rs == -1) {
                    return $this->responseErrorCustom("can not update product infor to cart", 500);
                }
            }

            //update cart information
            Cart::updateCart($cart_id);

            $cartInfor = [
                "empty"
            ];

            $userCart = Cart::getUserCart($user_id);
            $cartInfor = CartDetail::getCartDetail($cart_id);

            $returnString = [
                'cartInfor' => $cartInfor,
                'cart' => $userCart
            ];

            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function removeProductFromCart(Request $request)
    {
        /**
         * @SWG\Delete(
         *      path="/cart/remove",
         *      tags={"Carts"},
         *      description="Carts",
         *      summary="Carts",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="remove",
         *          description="remove product in cart",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="product_id",
         *                  type="string",
         *              ),
         *
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            //Remove product from cart
            $user_id = $request->user->id;
            $product_id = $request->product_id;

            $product = Product::productAvailable($product_id);
            if (!$product) {
                return $this->responseErrorCustom("product_notfound", 404);
            }

            $checkUserCart = Cart::userCartAvailable($user_id);

            //if user does not have any cart => create new cart
            if (!$checkUserCart) {
                $check = Cart::createUserCart($user_id);
                if ($check == -1) {
                    return $this->responseErrorCustom("can not create cart", 500);
                }
            }

            $userCart = Cart::userCartAvailable($user_id);
            $cart_id = $userCart->cart_id;

            $check = CartDetail::removeProductFromCartDetail($cart_id, $product_id);
            if (!$check) {
                return $this->responseErrorCustom("can not find product in cart", 500);
            }

            //update cart information
            Cart::updateCart($cart_id);

            $cartInfor = [
                "empty"
            ];

            $userCart = Cart::getUserCart($user_id);
            $cartInfor = CartDetail::getCartDetail($cart_id);

            $returnString = [
                'cartInfor' => $cartInfor,
                'cart' => $userCart
            ];

            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }
}
