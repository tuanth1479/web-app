<?php

namespace App\Http\Controllers;

use App\CollectEmail;
use Illuminate\Http\Request;

class CollectEmailController extends BaseApiController
{
    public function newEmailCollection(Request $request)
    {
        /**
         * @SWG\Post(
         *     path="/new-email-collection",
         *     description="",
         *     tags={"Email Collection"},
         *     summary="Email collection",
         *      @SWG\Parameter(
         *          name="body",
         *          description="Email collection",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="email",
         *                  type="string",
         *              ),
         *          ),
         *      ),
         *      @SWG\Response(response=200, description="Successful operation"),
         *      @SWG\Response(response=400, description="Bad request"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=403, description="Forbidden"),
         *      @SWG\Response(response=404, description="Not Found"),
         *      @SWG\Response(response=422, description="Unprocessable Entity"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         * )
         *
         */
        try {
            $validator = CollectEmail::validate($request->all(), 'Rule_Create');
            if ($validator) {
                return $this->responseErrorValidator($validator, 422);
            }

            $email = $request->email;
            $check = CollectEmail::where(['email' => $email])->first();

            if ($check) {
                if ($check->status == 1) {
                    return $this->responseErrorCustom('users_email_unique', 422);
                }
                $check->status = 1;
                $check->save();
                return $this->responseSuccess($check);
            }

            $newEmail = new CollectEmail();
            $newEmail->email = $email;
            $newEmail->status = 1;
            $newEmail->save();
            return $this->responseSuccess($newEmail);
        } catch (\Exception $e) {
            return $this->responseErrorException($e->getMessage(), 9999, 500);
        }
    }
}
