<?php

namespace App\Http\Controllers;

use App\Carousel;
use Illuminate\Http\Request;

class CarouselController extends BaseApiController
{
    public function get(Request $request)
    {
        /**
         * @SWG\Get(
         *      path="/carousel/get-all",
         *      tags={"Carousel"},
         *      description="Carousel",
         *      summary="Carousel",
         *
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $data = Carousel::select("*")->orderBy('id', 'desc')->get();
            return $this->responseSuccess($data);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function createCarousel(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/carousel/create",
         *      tags={"Carousel"},
         *      description="Carousel",
         *      summary="Carousel",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="Carouselpost",
         *          description="Carousel",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="title",
         *                  type="string",
         *              ),
         *              @SWG\Property(
         *                  property="image_link",
         *                  type="string",
         *              ),
         *              @SWG\Property(
         *                  property="link_to",
         *                  type="string",
         *              ),
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $carousel = new Carousel();

            $carousel->title = $request->title;
            $carousel->image_link = $request->image_link;
            $carousel->link_to = $request->link_to;
            $carousel->save();

            return $this->responseSuccess($carousel);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }
}
