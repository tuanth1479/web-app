<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
// use File;
// use League\Flysystem\File;
use Illuminate\Support\Facades\File;

class BaseApiController extends Controller
{
    protected  $statusCode;
    protected  $result = [];
    protected $apiError;

    public function __construct()
    {
        $this->result = [
            'error' => false,
            'data' => null,
            'errors' => []
        ];
        $this->statusCode = 200;
        $this->apiError = Lang::get('errorCodeApi');
    }
    private function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }
    private function setErrorResult($errors)
    {
        $this->result['error'] = true;
        $this->result['errors'] = $errors;
        return $this;
    }
    private function setSuccessResult($data)
    {
        $this->result['data'] = $data;
        return $this;
    }
    public function responseSuccess($data = array())
    {
        $this->setSuccessResult($data);
        return response()->json($this->result, $this->statusCode);
    }
    public function responseErrorValidator($errors = array(), $httpcode = 500)
    {
        $this->setStatusCode($httpcode);
        $this->setErrorResult($errors);
        return response()->json($this->result, $this->statusCode);
    }

    public function responseErrorException($errorMessage, $apicode = 99999, $httpcode = 500)
    {
        $this->setStatusCode($httpcode);
        $errors[0] = [
            'errorCode' => $apicode,
            'errorMessage' => $errorMessage
        ];
        $this->setErrorResult($errors);
        return response()->json($this->result, $this->statusCode);
    }
    public function responseErrorCustom($errorCode, $httpcode = 500)
    {
        $this->setStatusCode($httpcode);
        $errors[0] = [
            'errorCode' => $this->apiError['ApiErrorCodes'][$errorCode],
            'errorMessage' => $this->apiError['ApiErrorMessages'][$errorCode]
        ];

        $this->setErrorResult($errors);
        return response()->json($this->result, $this->statusCode);
    }

    public function saveImage($request, $type, $oldPath = null)
    {
        $default = 'assets/server/img/default.png';
        switch ($type) {
            case "user": {
                    $saveFolder = 'assets/server/user/img/';
                    break;
                }
            case "category": {
                    $saveFolder = 'assets/server/img/category/';
                    break;
                }
            case "product": {
                    $saveFolder = 'assets/server/img/product/';
                    break;
                }

            default:
                $saveFolder = 'assets/';
        }
        if ($request->hasFile('image')) {
            $file = $request->file('image');

            //create an unique image name, recreate if find out that name is duplicated
            $imageName = time() . '_' . str_random(4) . '.' . $file->getClientOriginalExtension();
            while (file_exists($saveFolder . $imageName)) {
                $imageName = time() . '_' . str_random(4) . '.' . $file->getClientOriginalExtension();
            }

            //save image to disk
            $file->move($saveFolder, $imageName);

            //check if available an old image (except default.png), delete it from disk
            $defaultPath = $saveFolder . 'default.png';
            if (($oldPath != $defaultPath)) {
                // $oldPath = public_path($oldPath);
                if (File::exists($oldPath)) {
                    File::delete($oldPath);
                }
            }

            return $saveFolder . $imageName;
        }
        return $default;
    }

    public function filterResult($request, $result)
    {
        if (isset($request->category_id)) {
            $result = $result->where('category_id', "like", $request->category_id);
        }

        if (isset($request->gender)) {
            $result = $result->where('product_gender', "like", $request->gender);
        }

        if (isset($request->min_price)) {
            $result = $result->where('product_price', ">=", $request->min_price);
        }

        if (isset($request->max_price)) {
            $result = $result->where('product_price', "<=", $request->max_price);
        }
    }
}
