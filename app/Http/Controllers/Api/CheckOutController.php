<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Cart;
use Illuminate\Http\Request;

class CheckOutController extends BaseApiController
{

    public function checkout(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/checkout/user-cart",
         *      tags={"Checkout"},
         *      description="Checkout",
         *      summary="Checkout",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="export",
         *          description="",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="cart_id",
         *                  type="integer",
         *              ),
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            //generate bill and switch cart's status = 1
            $user_id = $request->user->id;


            $cart = Cart::userCartAvailable($user_id);

            if (!$cart)
                return $this->responseErrorCustom("can not find user cart", 404);

            $cart_id = $cart->cart_id;

            $check = Cart::checkoutCart($cart_id);
            if ($check == -1) {
                return $this->responseErrorCustom("can not checkout cart", 404);
            }
            $check = Bill::createBill($user_id, $cart_id);
            if ($check == -1) {
                Cart::returnCartState($user_id, $cart_id);
                return $this->responseErrorCustom("can not create bill", 404);
            }

            $bill = Bill::getBill($user_id, $cart_id);
            $returnString = [
                'billInfor' => $bill,
                'userInfor' => $request->user
            ];

            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function finishedShip(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/checkout/finished",
         *      tags={"Checkout"},
         *      description="Checkout",
         *      summary="Checkout",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="export",
         *          description="",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="validate_code",
         *                  type="string",
         *              ),
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            //finish payment and close cart, bill
            $user_id = $request->user->id;
            $validateCode = $request->validate_code;

            $bill = Bill::getBillOfValidateCode($validateCode);
            if (!$bill)
                return $this->responseErrorCustom("can not validate bill", 404);

            $billId = $bill->id;
            Bill::finished($billId);

            $returnString = [
                'billInfor' => $bill,
                'userInfor' => $request->user
            ];

            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }

    }

    public function cancelShip(Request $request)
    {
        //delete cart and bill
    }
}
