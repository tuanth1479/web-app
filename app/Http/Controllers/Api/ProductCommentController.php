<?php

namespace App\Http\Controllers;

use App\ProductComment;
use Illuminate\Foundation\Console\Presets\React;
use Illuminate\Http\Request;

class ProductCommentController extends BaseApiController
{
    public function getProductComment(Request $request)
    {
        //product_id

        /**
         * @SWG\Get(
         *      path="/product-comment/{product_id}",
         *      tags={"ProductComment"},
         *      description="ProductComment",
         *      summary="ProductComment",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *       @SWG\Parameter(
         *         description="ID ProductComment",
         *         in="path",
         *         name="product_id",
         *         required=true,
         *         type="string",
         *         format="int64"
         *     ),
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $product_id = $request->product_id;

            $productComment = [
                "empty"
            ];

            $productComment = ProductComment::get($product_id);
            if (!$productComment) {
                return $this->responseErrorCustom("cart not found product comment", 404);
            }

            $returnString = [
                'productComment' => $productComment,
            ];

            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function postProductComment(Request $request)
    {
        //product id
        //user id

        /**
         * @SWG\Post(
         *      path="/product-comment/post",
         *      tags={"ProductComment"},
         *      description="ProductComment",
         *      summary="ProductComment",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="post",
         *          description="ProductComment",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\property(
         *                  property="product_id",
         *                  type="string",
         *              ),
         *              @SWG\Property(
         *                  property="content",
         *                  type="string",
         *              ),
         *
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {

            $userId = $request->user->id;
            $productId = $request->product_id;
            $content = $request->content;

            $c = ProductComment::post($productId, $content, $userId);
            if ($c == -1) {
                return $this->responseErrorCustom('can not create comment', 500);
            }

            $productComment = ProductComment::get($productId);
            $returnString = [
                'user' => $request->user,
                'commnent' => $productComment
            ];
            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function updateProductComment(Request $request)
    {
        //product id
        //comment id
    }

    public function deleteProductComment(Request $request)
    {
        //product id
        //comment id
    }
}
