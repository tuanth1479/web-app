<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\VisitedHistory;
use phpDocumentor\Reflection\Types\Null_;

class ProductController extends BaseApiController
{

    public function getProduct(Request $request)
    {
        /**
         * @SWG\Get(
         *      path="/product/{id}",
         *      tags={"Products"},
         *      description="Get infor of one product through product_id",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *      @SWG\Parameter(
         *         description="ID product to show",
         *         in="path",
         *         name="id",
         *         required=true,
         *         type="string",
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */

        try {
            $result = Product::select('*')
                ->where(['product_id' => $request->id])
                ->first();

            //add 1 view for product id to "visited_history" table
            $visited = new VisitedHistory();

            $check = Product::where(['product_id' => $request->id])->first();
            if (!$check) {
                return $this->responseErrorCustom('product_id_not_found', 404);
            }
            $visited->product_id = $request->id;
            $visited->save();

            return $this->responseSuccess($result);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    // public function getAllProducts(Request $request)
    // {
    //
    //

    //     try {
    //         $result = Product::paginate(10)->withPath("/get/all?page");
    //         return $this->responseSuccess($result);
    //     } catch (\Exception $exception) {
    //         return $this->responseErrorException($exception->getMessage(), 9999, 500);
    //     }
    // }

    public function getProductsOfCategory(Request $request)
    {
        /**
         * @SWG\Get(
         *      path="/product/get/all?category_id={category_id}&page={page}&gender={gender}",
         *      tags={"Products"},
         *      description="Get list products Should test by Postman",
         *      summary="Get list products Should test by Postman",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *      @SWG\Parameter(
         *         description="page",
         *         in="path",
         *         name="page",
         *         required=false,
         *         type="integer",
         *      ),
         *
         *      @SWG\Parameter(
         *         description="gender filter",
         *         in="path",
         *         name="gender",
         *         required=false,
         *         type="string",
         *      ),
         *
         *      @SWG\Parameter(
         *         description="cat filter",
         *         in="path",
         *         name="category_id",
         *         required=false,
         *         type="string",
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */

        try {
            static $num_product_per_page = 10;
            $result = "null";

            $result = Product::select("*");

            $this->filterResult($request, $result);

            $result = $result->paginate($num_product_per_page)->withPath("/get/all");

            return $this->responseSuccess($result);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function createProduct(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/product/create",
         *      tags={"Products"},
         *      description="Carousel",
         *      summary="Carousel",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *      @SWG\Parameter(
         *          name="product_id",
         *          description="Category's id",
         *          in="formData",
         *          type="string",
         *          required=true,
         *      ),
         *      @SWG\Parameter(
         *          name="name",
         *          description="Category's name",
         *          in="formData",
         *          type="string",
         *          required=true,
         *      ),
         *      @SWG\Parameter(
         *          name="category_id",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         * @SWG\Parameter(
         *          name="product_color",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="product_size",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="product_price",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="number",
         *          format="double",
         *      ),
         *      @SWG\Parameter(
         *          name="price_unit",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="product_gender",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="technical_data",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="brand",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="description",
         *          description="Category's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="image",
         *          description="Image of Category",
         *          in="formData",
         *          type="file",
         *      ),
         *
         *
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $product = new Product();

            $product->product_id = $request->product_id;
            $product->name = $request->name;
            $product->category_id = $request->category_id;
            $product->product_color = $request->product_color;
            $product->product_size = $request->product_size;
            $product->product_price = $request->product_price;
            $product->price_unit = $request->price_unit;
            $product->product_gender = $request->product_gender;
            $product->technical_data = $request->technical_data;
            $product->brand = $request->brand;
            $product->description = $request->description;

            $link = $this->saveImage($request, 'product', NULL);
            $product->image_link = $link;

            $product->save();

            return $this->responseSuccess($product);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function updateProduct(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/product/update/{product_id}",
         *      tags={"Products"},
         *      description="Carousel",
         *      summary="Carousel",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *      @SWG\Parameter(
         *         description="ID product to update",
         *         in="path",
         *         name="product_id",
         *         required=true,
         *         type="string",
         *      ),
         *
         *      @SWG\Parameter(
         *          name="name",
         *          description="Product's name",
         *          in="formData",
         *          type="string",
         *          required=true,
         *      ),
         *      @SWG\Parameter(
         *          name="category_id",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         * @SWG\Parameter(
         *          name="product_color",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="product_size",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="product_price",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="number",
         *          format="double",
         *      ),
         *      @SWG\Parameter(
         *          name="price_unit",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="product_gender",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="technical_data",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="brand",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="description",
         *          description="Product's Description",
         *          in="formData",
         *          required=true,
         *          type="string",
         *      ),
         *      @SWG\Parameter(
         *          name="image",
         *          description="Image of Product",
         *          in="formData",
         *          type="file",
         *      ),
         *
         *
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $product_id = $request->product_id;

            $product = Product::where(['product_id' => $product_id])->first();
            if (!$product) {
                return $this->responseErrorCustom('product not found', 404);
            }

            //check valid name to update
            $check = Product::where(['name' => $request->name])->first();
            if ($check) {
                if ($check->id != $request->cat_id) {
                    return $this->responseErrorCustom('product name', 422);
                }
            }

            $link = $this->saveImage($request, 'product', $product->image_link);

            //save to database
            $check = Product::where(['product_id' => $product_id])->update([
                'name' => $request->name,
                'image_link' => $link,
                'category_id' => $request->category_id,
                'product_color' => $request->product_color,
                'product_size' => $request->product_size,
                'product_price' => $request->product_price,
                'price_unit' => $request->price_unit,
                'product_gender' => $request->product_gender,
                'technical_data' => $request->technical_data,
                'brand' => $request->brand,
                'description' => $request->description,
            ]);

            if($check != 1){
                return $this->responseErrorCustom('can not update', 500);
            }

            $productInfor = Product::where(['product_id' => $product_id])->first();

            return $this->responseSuccess($productInfor);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }
}
