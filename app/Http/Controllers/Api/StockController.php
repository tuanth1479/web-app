<?php

namespace App\Http\Controllers;

use App\Product;
use App\Stock;
use App\StockExport;
use App\StockImport;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StockController extends BaseApiController
{
    public function importStock(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/stock/import",
         *      tags={"Stocks"},
         *      description="Stock",
         *      summary="Stock",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="import",
         *          description="stock import",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="product_id",
         *                  type="string",
         *              ),
         *              @SWG\property(
         *                  property="quantity",
         *                  type="integer",
         *              ),
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            $importStock = new StockImport();

            //check product_id
            $checkProduct = Product::where(['product_id' => $request->product_id])->first();

            if (!$checkProduct) {
                return $this->responseErrorCustom("notfound_product", 404);
            }


            $importStock->product_id = $request->product_id;

            if ($request->quantity < 1) {
                return $this->responseErrorCustom("quantity_product 0", 401);
            }
            $importStock->quantity = $request->quantity;
            $importStock->import_date = Carbon::now();

            $importStock->save();

            //stock

            $stock = '';
            $checkStock = Stock::where(['product_id' => $request->product_id])->first();
            if ($checkStock) {
                $changeQuantity = $checkStock->quantity;
                $changeQuantity += $request->quantity;
                $checkStock->quantity = $changeQuantity;
                $checkStock->save();

                $stock = $checkStock;
            } else {
                $stock = new Stock();
                $stock->product_id = $request->product_id;
                $stock->quantity = $request->quantity;
                $stock->save();
            }
            $returnString = [
                'importStock' => $importStock,
                'stock' => $stock
            ];
            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }

    public function exportStock(Request $request)
    {
        /**
         * @SWG\Post(
         *      path="/stock/export",
         *      tags={"Stocks"},
         *      description="Stock",
         *      summary="Stock",
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *       @SWG\Parameter(
         *          name="export",
         *          description="stock export",
         *          required=true,
         *          in="body",
         *          @SWG\Schema(
         *              @SWG\Property(
         *                  property="product_id",
         *                  type="string",
         *              ),
         *              @SWG\property(
         *                  property="quantity",
         *                  type="integer",
         *              ),
         *          ),
         *      ),
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {

            $exportStock = new StockExport();
            // return $request->product_id;

            //check product_id
            $checkProduct = Product::where(['product_id' => $request->product_id])->first();
            if (!$checkProduct) {
                return $this->responseErrorCustom("notfound_product", 404);
            }


            $exportStock->product_id = $request->product_id;

            if ($request->quantity < 1) {
                return $this->responseErrorCustom("quantity_product 0", 401);
            }
            $exportStock->quantity = $request->quantity;
            $exportStock->export_date = Carbon::now();


            //stock
            $stock = '';
            $checkStock = Stock::where(['product_id' => $request->product_id])->first();
            if ($checkStock) {
                $changeQuantity = $checkStock->quantity;
                $changeQuantity = $changeQuantity - $request->quantity;
                if ($changeQuantity < 0) {
                    return $this->responseErrorCustom( "negative product quantity", 401);
                }
                $checkStock->quantity = $changeQuantity;
                $checkStock->save();

                $stock = $checkStock;
            } else {
                return $this->responseErrorCustom("please import product", 401);
            }
            //if error occurs not save
            $exportStock->save();

            $returnString = [
                'exportStock' => $exportStock,
                'stock' => $stock
            ];
            return $this->responseSuccess($returnString);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }
}
