<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends BaseApiController
{
    public function getProfile(Request $request)
    {
        /**
         * @SWG\Get(
         *      path="/user/profile",
         *      tags={"Users"},
         *      description="Get user profile",
         *
         *      security={
         *       {"jwt": {"*"}},
         *      },
         *
         *
         *      @SWG\Response(response=200, description="Successful"),
         *      @SWG\Response(response=401, description="Unauthorized"),
         *      @SWG\Response(response=500, description="Internal Server Error"),
         *     )
         */
        try {
            return $this->responseSuccess($request->user);
        } catch (\Exception $exception) {
            return $this->responseErrorException($exception->getMessage(), 9999, 500);
        }
    }
}
