<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{

    public static function getBillOfValidateCode($validateCode)
    {
        try {
            return Bill::where(['validate_code' => $validateCode])->first();
        } catch (Exception $e) {
            return -1;
        }
    }

    public static function finished($billId)
    {
        try {
            Bill::where(['id' => $billId])
            ->update([
                'status' => 1,
            ]);
            return 1;
        } catch (Exception $e) {
            return -1;
        }
    }

    public static function getBill($userID, $cartID)
    {
        try {
            return Bill::where([
                'user_id' => $userID,
                'cart_id' => $cartID
            ])->first();
        } catch (Exception $e) {
            return -1;
        }
    }

    public static function createBill($userID, $cartID)
    {
        try {
            $bill = new Bill();
            $bill->user_id = $userID;
            $bill->cart_id = $cartID;
            $bill->status = 0;

            $uniqueCode = $userID . $cartID . random_int(0, 9999);

            $bill->validate_code = $uniqueCode;

            $bill->save();
            return 1;
        } catch (Exception $e) {
            return -1;
        }
    }
}
