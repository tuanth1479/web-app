<?php

namespace App;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    public static function getCartDetail($cart_id)
    {
        try {
            return CartDetail::select('*')->where(['cart_id' => $cart_id])->get();
        } catch (\Exception $e) {
            return "can not get cart infor getCartDetail function";
        }
    }

    public static function createProductInforToCartDetail($cart_id, $product_id, $productPrice)
    {
        try {
            $cartDetail = new CartDetail();
            $cartDetail->cart_id = $cart_id;
            $cartDetail->product_id = $product_id;
            $cartDetail->quantity = 1;
            $cartDetail->product_price = $productPrice;
            $cartDetail->save();
            return 1;
        } catch (\Exception $e) {
            return -1;
        }
    }

    public static function updateProductInforToCartDetail($cart_id, $product_id, $productPrice, $isIncrease)
    {

        try {
            if ($isIncrease == 1) {
                CartDetail::where([
                    'cart_id' => $cart_id,
                    'product_id' => $product_id
                ])
                    ->increment(
                        'quantity',
                        1,
                        [
                            'product_price' => $productPrice
                        ]
                    );
            } else {

                CartDetail::where([
                    'cart_id' => $cart_id,
                    'product_id' => $product_id
                ])
                    ->decrement(
                        'quantity',
                        1,
                        [
                            'product_price' => $productPrice
                        ]
                    );
            }
            return 1;
        } catch (\Exception $e) {
            return -1;
        }
    }

    public static function checkIsProductAvailableInCartDetail($cart_id, $product_id)
    {
        try {
            $check = CartDetail::where([
                'cart_id' => $cart_id,
                'product_id' => $product_id
            ])->first();
            if ($check) {
                return 1;
            } else return -1;
        } catch (\Exception $e) {
            return -1;
        }
    }

    public static function removeProductFromCartDetail($cart_id, $product_id)
    {
        try {
            return CartDetail::where([
                'cart_id' => $cart_id,
                'product_id' => $product_id
            ])->delete();
        } catch (\Exception $e) {
            return -1;
        }
    }
}
