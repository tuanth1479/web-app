<?php

namespace App;

use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //cartAvailable is cart have status == 0;
    public static function userCartAvailable($user_id)
    {
        return Cart::where([
            'user_id' => $user_id,
            'status' => 0
        ])
            ->first();
    }

    public static function createUserCart($user_id)
    {
        try {
            $cart = new Cart();
            $cart->user_id = $user_id;
            $cart->status = 0;
            $cart->total_price = 0;
            $cart->save();
            return 1;
        } catch (\Exception $e) {
            return -1;
        }
    }

    public static function getUserCart($user_id)
    {
        try {
            return Cart::where([
                'user_id' => $user_id,
                'status' => 0
            ])->first();
        } catch (\Exception $e) {
            return "can not get user cart getUserCart function";
        }
    }

    public static function updateCart($cart_id)
    {
        try {
            $totalPrice = CartDetail::select(
                DB::raw("SUM(quantity*product_price) as Total_Price")
            )
                ->where(['cart_id' => $cart_id])->get()->toArray();

            $totalPrice = $totalPrice["0"]["Total_Price"];
            if ($totalPrice == null)
                $totalPrice = 0;
            // cập nhật giá total
            return Cart::where(['cart_id' => $cart_id])
                ->update([
                    'total_price' => $totalPrice
                ]);
        } catch (\Exception $e) {
            return "can not update card detail. Please Check & Update Again.";
        }
    }

    public static function checkoutCart($cart_id)
    {
        try {
            Cart::where(['cart_id' => $cart_id])
                ->update([
                    'status' => 1
                ]);

            return 1;
        } catch (\Exception $e) {
            return "can not checkout user cart checkoutCart function";
        }
    }

    public static function returnCartState($userID, $cartId)
    {
        try {
            Cart::where([
                'cart_id' => $cartId,
                'user_id' => $userID

            ])
                ->update([
                    'status' => 0
                ]);

            return 1;
        } catch (\Exception $e) {
            return "can not checkout user cart checkoutCart function";
        }
    }
}
