<?php

namespace App;

use Exception;;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    public static function get($productId)
    {
        try {
            return ProductComment::select("*")->where(['product_id' => $productId])->get();
        } catch (Exception $e) {
            return -1;
        }
    }

    public static function post($productId, $content, $userId)
    {
        try {
            $prComment = new ProductComment;
            $prComment->content = $content;
            $prComment->product_id = $productId;
            $prComment->user_id = $userId;
            $prComment->save();
            return 1;
        } catch (Exception $e) {
            return -1;
        }
    }
}
