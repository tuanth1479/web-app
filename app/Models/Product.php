<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'product_id', 'visited_date',
    ];
    protected $hidden = [
        'id',
    ];

    public static function productAvailable($product_id)
    {
        try {
            return Product::where(['product_id' => $product_id])->first();
        } catch (\Exception $e) {
            return -1;
        }
    }
}
