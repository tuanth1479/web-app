<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitedHistory extends Model
{
    protected $table = 'visited_history';
    protected $fillable = [
        'product_id', 'visited_date',
    ];
    protected $hidden = [
        'id',
    ];
}
