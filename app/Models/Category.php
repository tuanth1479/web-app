<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends BaseModel
{
    protected $table = 'category';
    protected $fillable = [
        'name', 'description', 'image_link',
    ];

    public static function checkConstraint($name)
    {
        return Category::where(['name' => $name])->first();
    }

    public static $rules = array(
        'Rule_Create_Category' => [
            'name' => 'required|string|max:32',
            'description' => 'required|string|max:256',
            'image' => 'required_without:description,name|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ],
        'Rule_Update_Category' => [
            'name' => 'required|string|max:32',
            'description' => 'required|string|max:256',
            'image' => 'required_without:description,name|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ],
    );
}
