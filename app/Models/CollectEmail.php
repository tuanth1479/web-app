<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectEmail extends BaseModel
{
    protected $table = 'collect_emails';
    protected $fillable = [
        'email',
    ];

    public static $rules = array(
        'Rule_Create' => [
            'email' => 'required|regex:/^[a-z][a-z0-9_\.]{2,}@[a-z0-9]{2,}(\.[a-z0-9]{2,}){1,2}$/',
        ],
    );
}
