<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unique();
            //login field
            $table->string('username', 32)->require();
            $table->string('email', 32)->unique()->require();
            $table->string('password', 256)->require();
            //more info
            $table->enum('gender', ['male', 'female'])->default('male');
            //contact
            $table->string('first_name', 32)->require();
            $table->string('last_name', 32)->require();
            $table->string('country', 20)->default('undefined');
            $table->string('city_province', 20)->default('undefined');
            $table->string('district', 20)->default('undefined');
            $table->string('ward',20)->default('undefined');
            $table->string('commune', 20)->default('undefined'); //village
            $table->string('street', 20)->default('undefined');
            $table->string('address_detail', 20)->default('undefined'); //house number, building, apartment

            $table->string('phone', 32)->default('undefined');

            //priviledge: admin 1 - customer 0
            $table->integer('admin')->default(0);
            $table->integer('active')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
