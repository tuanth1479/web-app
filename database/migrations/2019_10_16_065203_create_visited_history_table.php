<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitedHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visited_history', function (Blueprint $table) {
            $table->increments('id')->unique();
            // $table->integer('user_id')->unsigned();
            $table->string('product_id', 20);
            $table->timestamps();

            $table->foreign('product_id')->references('product_id')->on('products');
            // $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visited_history');
    }
}
