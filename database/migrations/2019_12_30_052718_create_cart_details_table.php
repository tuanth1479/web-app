<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_details', function (Blueprint $table) {
            $table->integer('cart_id')->unsigned();
            $table->string('product_id', 20);
            $table->integer('quantity')->unsigned();
            $table->double('product_price')->unsigned();

            $table->timestamps();

            $table->primary(['cart_id', 'product_id']);

            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('cart_id')->references('cart_id')->on('carts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_details');
    }
}
