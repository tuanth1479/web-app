<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('order_details', function (Blueprint $table) {
    //         $table->increments('id')->unique();
    //         $table->integer('order_id')->unsigned();
    //         $table->string('product_id', 20);

    //         $table->integer('quantity')->unsigned();
    //         $table->double('list_price')->unsigned();
    //         $table->double('discount')->unsigned();
    //         $table->double('final_price')->default(0)->unsigned();
    //         $table->timestamps();

    //         //forein key
    //         $table->foreign('order_id')->references('id')->on('orders');
    //         $table->foreign('product_id')->references('product_id')->on('products');
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    // public function down()
    // {
    //     Schema::dropIfExists('order_details');
    // }
}
