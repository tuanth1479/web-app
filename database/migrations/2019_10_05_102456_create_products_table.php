<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id')->unique();

            $table->string('product_id', 20)->unique()->require(); //NAME_SIZE_GENDER
            $table->string('name', 32)->require();

            //foreign key
            $table->string('category_id', 20)->require();

            $table->string('image_link', 256)->default('undefined'); //path of the image
            //base infor
            $table->string('product_color')->default('undefined');
            $table->string('product_size')->default('undefined');
            $table->double('product_price')->default(0);
            $table->string('price_unit', 10)->default('undefined');
            $table->string('product_gender')->default('undefined'); //for male, female, all

            $table->string('technical_data')->default('undefined');
            $table->string('brand', 16)->default('undefined');
            $table->string('description', 256)->default('undefined');
            $table->timestamps();

            //foreign key
            $table->foreign('category_id')->references('category_id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
