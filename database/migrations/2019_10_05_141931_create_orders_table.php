<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('orders', function (Blueprint $table) {
    //         $table->increments('id')->unique();
    //         $table->integer('user_id')->unsigned();
    //         $table->boolean('status')->default(0);
    //         $table->double('total_price')->default(0)->unsigned();
    //         $table->timestamp('order_date')->useCurrent();
    //         $table->timestamp('required_date')->useCurrent();
    //         $table->timestamp('shipped_date')->useCurrent();
    //         $table->timestamps();

    //         $table->foreign('user_id')->references('id')->on('users');
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    // public function down()
    // {
    //     Schema::dropIfExists('orders');
    // }
}
