<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'username' => 'Admin',
                "first_name" => "User first name",
                "last_name" => "User last name",
                'email' => 'admin@gmail.com',
                'password' => bcrypt('string'),
                'phone' => '0947965225',
                'active' => 1,
                'admin' => 1,
            ],
            [
                "username" => "User",
                "first_name" => "User first name",
                "last_name" => "User last name",
                "email" => "user@gmail.com",
                'password' => bcrypt('string'),
                'phone' => '035388175',
                "admin" => 0,
                "active" => 1,
            ],

        ];

        foreach ($users as $key => $user) {
            User::create($user);
        }
    }
}
