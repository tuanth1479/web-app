<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            [
                'category_id' => 'cat_1',
                'name' => 'Quần',
                'image_link' => 'assets/server/img/default.png',
                'description' => 'Quần cho nam',

            ],
            [
                'category_id' => 'cat_2',
                'name' => 'Áo',
                'image_link' => 'assets/server/img/default.png',
                'description' => 'Áo cho nam',

            ],
        ];

        foreach ($category as $key => $cat) {
            Category::create($cat);
        }
    }
}
