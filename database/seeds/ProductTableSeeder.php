<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'product_id' => 'Sotay',
                'category_id' => 'cat_2',
                'name' => 'So tay',
                'image_link' => 'assets/server/img/default.png',
                'brand' => 'HCMC',
                'description' => '0947965225',
            ],
            [
                'product_id' => 'Ao',
                'category_id' => 'cat_1',
                'name' => 'admin@gmail.com',
                'image_link' => 'assets/server/img/default.png',
                'brand' => 'Binh Dinh',
                'description' => '03030303031',
            ],
        ];

        foreach ($products as $key => $product) {
            Product::create($product);
        }
    }
}
