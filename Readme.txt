//Tạo file env từ env-example
copy file env example và sửa tên thành ".env"
cấu hình 1 số thông số cần thiết trong file .env vừa tạo
    - Database
    - Email port

//Một số câu lệnh cần chạy tiếp theo
composer install //Cài đặt dependencies
php artisan key:generate //Tạo key cho App
php artisan jwt:secret //JWT
php artisan migrate:refresh --seed //để tạo database



nội dung file .env

APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:AUFrZdB/kTQANEzyQSOhCKKeyM4Qye8SNszhIXabmUE=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=web-app
DB_USERNAME=root
DB_PASSWORD=''

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

SWAGGER_VERSION=2.0
L5_SWAGGER_GENERATE_ALWAYS=true
MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"


JWT_SECRET=t35YbhQc2ncD3IdZ9UF4sdHYvi35ToOvKJStEFkVTaqraqGG27vQNGQ7q4XW84Dy

MAIL_ENCRYPTION=ssl
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=tuanth.cnc.uit@gmail.com
MAIL_PASSWORD=fillhere
MAIL_FROM=tuanth.cnc.uit@gmail.com
MAIL_NAME=HoangTuan

