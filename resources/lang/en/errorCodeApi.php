<?php

$ApiErrorMessages = array(
    //user
    'unauthorized' => 'The account is unauthorized.',
    'userId_required' => 'The user ID is required',
    'userId_integer' => 'The user ID is integer',
    'userId_min' => 'The user ID min is 1',
    'can_not_delete_user' => 'Can not delete user. Minimum admin is 1',
    'can_not_edit_user' =>  'Can not edit user. Minimum admin is 1',
    'middleware_users_not_found' => "The user was deleted from the system",
    'username_string' => 'The username must be string',

    'active_required' => 'The active field is required',
    'active_boolean' => 'The active field is boolean',

    'avatar_required' => 'The avatar is required',
    'avatar_image' => 'The avatar must be an image',
    'avatar_mime' => 'The avatar must be "jpeg,png,jpg,gif,svg" file',
    'avatar_max' => 'The size of avatar is too large (>2MB)',

    // / Validator Errors Message /
    'users_not_found' => 'The user is not found.',
    'users_not_create' => 'The user is not create.',
    'users_name_required' => 'The name is required.',
    'users_name_min' => 'The name must be at least 3 characters',
    'users_name_max' => 'The name may not be greater than 30 characters',
    'users_email_required' => 'The email is required.',
    'users_email_unique' => 'The email has already been taken.',
    'users_email_string' => 'The email must be a string.',
    'users_email_regex' => 'The email format is invalid.',
    'users_password_required' => 'The password is required.',
    'users_password_min' => 'The password must be at least 6 characters.',
    'users_password_max' => 'The password may not be greater than 16 characters.',
    'users_password_incorrect' => 'The password is incorrect.',
    'users_password_string' => 'The password must be a string.',
    'users_newPassword_required' => 'The new password is required.',
    'users_newPassword_min' => 'The new password must be at least 6 characters.',
    'users_newPassword_max' => 'The new password may not be greater than 16 characters.',
    'users_newPassword_string' => 'The new password must be a string.',
    'users_confirmNewPassword_required' => 'The confirm new password again is required.',
    'users_confirmNewPassword_min' => 'The confirm new password again must be at least 6 characters.',
    'users_confirmNewPassword_max' => 'The confirm new password again may not be greater than 16 characters.',
    'users_confirmNewPassword_string' => 'The confirm new password again must be a string.',
    'users_confirmNewPassword_same' => 'The confirm new password and new password must match.',
    'users_confirmPassword_required' => 'The confirm password again is required.',
    'users_confirmPassword_min' => 'The confirm password again must be at least 6 characters.',
    'users_confirmPassword_max' => 'The confirm password again may not be greater than 16 characters.',
    'users_confirmPassword_string' => 'The confirm password again must be a string.',
    'users_confirmPassword_same' => 'The confirm password and password must match.',
    'users_currentPassword_required' => 'The current password is required.',
    'users_currentPassword_min' => 'The current password must be at least 6 characters.',
    'users_currentPassword_incorrect' => 'The current password is incorrect',
    'users_currentPassword_max' => 'The current password may not be greater than 16 characters.',
    'users_currentPassword_string' => 'The current password must be a string.',
    'user_email_or_password_incorrect' => "The email or password is incorrect",
    'friend_not_found' => 'The friend is not found',
    'admin_required' => 'The admin field is required',
    'admin_boolean' => 'The admin field must be boolean value',

    // / Token Errors Message /
    'tokens_not_found' => 'The token is not found.',
    'tokens_not_create' => 'The token is not create.',
    'tokens_expired' => 'The token is expired.',
    'tokens_token_required' => 'The token is required.',
    'tokens_invalid' => 'The token is invalid.',
    'tokens_invalid_credentials' => 'The token is invalid credentials',

    'tokens_reset_password_not_found' => 'The token is not found.',
    'tokens_reset_password_not_create' => 'The token is not create.',
    'tokens_reset_password_expired' => 'The token is expired.',
    'tokens_reset_password_token_required' => 'The token is required.',
    'tokens_reset_password_invalid' => 'The token is invalid.',
    'tokens_reset_password_invalid_credentials' => 'The token is invalid credentials',

    'tokens_create_user_invalid' => 'The token is invalid. Please check email again',

    //filter
    'offset_integer' => 'Offset must be integer',
    'limit_integer' => 'Limit must be integer',
    'sort_in' => 'Sort in asc or desc',
    'fieldSort_string' => 'fieldSort must be string',
    'TypeSort_in' => 'TypeSort in asc or desc',
    'fieldSearch_string' => 'fieldSearch must be string',
    'keySearch_string' => 'keySearch must be string',
    //custom error
    'email_password_invalid' => 'The email or password are invalid',
    'user_password_invalid' => 'The password is invalid', //when enter current password in change password
    'account_deactive' => 'Your account has been blocked or deactivated',
    'users_available' => 'The user has been available', //when send request create user
    'user_priority' => 'Permission denied',
    'fieldSort_not_found' => 'Field Sort not found',
    'fieldSearch_not_found' => 'Field Search not found',
);

$ApiErrorCodes = array(

    //user
    'unauthorized' => 1000,
    'userId_required' => 1001,
    'userId_integer' => 1002,
    'userId_min' => 1003,
    'can_not_delete_user' => 1004,
    'can_not_edit_user' => 1005,
    'middleware_users_not_found' => 1006,

    'avatar_required' => 1007,
    'avatar_image' => 1008,
    'avatar_mime' => 1009,
    'avatar_max' => 1010,

    'active_required' => 1011,
    'active_boolean' => 1012,

    // / Validator Errors code: using 2xxx /
    'users_not_found' => 2001,
    'users_not_create' => 2002,
    'users_name_required' => 2010,
    'users_name_min' => 2011,
    'users_name_max' => 2012,
    'users_email_required' => 2020,
    'users_email_unique' => 2022,
    'users_email_string' => 2023,
    'users_email_regex' => 2024,
    'users_password_required' => 2040,
    'users_password_min' => 2041,
    'users_password_max' => 2042,
    'users_password_incorrect' => 2043,
    'users_password_string' => 2044,
    'users_newPassword_required' => 2050,
    'users_newPassword_min' => 2051,
    'users_newPassword_max' => 2052,
    'users_newPassword_string' => 2053,
    'users_confirmNewPassword_min' => 2061,
    'users_confirmNewPassword_max' => 2062,
    'users_confirmNewPassword_string' => 2063,
    'users_confirmNewPassword_required' => 2064,
    'users_confirmNewPassword_same' => 2065,
    'users_confirmPassword_min' => 2066,
    'users_confirmPassword_max' => 2067,
    'users_confirmPassword_string' => 2068,
    'users_confirmPassword_required' => 2069,
    'users_confirmPassword_same' => 2070,
    'users_currentPassword_required' => 2071,
    'users_currentPassword_min' => 2072,
    'users_currentPassword_max' => 2073,
    'users_currentPassword_string' => 2074,
    'users_currentPassword_incorrect' => 2075,
    'user_email_or_password_incorrect' => 2076,
    'admin_required' => 2077,
    'admin_boolean' => 2078,

    // / Token Errors code: using 30xx /
    'tokens_not_found' => 3000,
    'tokens_not_create' => 3001,
    'tokens_expired' => 3002,
    'tokens_token_required' => 3003,
    'tokens_invalid' => 3004,
    'tokens_invalid_credentials' => 3005,

    'tokens_reset_password_not_found' => 3050,
    'tokens_reset_password_not_create' => 3051,
    'tokens_reset_password_expired' => 3052,
    'tokens_reset_password_token_required' => 3053,
    'tokens_reset_password_invalid' => 3054,
    'tokens_reset_password_invalid_credentials' => 3055,

    'tokens_create_user_invalid' => 3056,

    //filter
    'offset_integer' => 3341,
    'limit_integer' => 3342,
    'sort_in' => 3343,
    'fieldSort_string' => 3351,
    'TypeSort_in' => 3361,
    'fieldSearch_string' => 3371,
    'keySearch_string' => 3381,

    //custom error
    'email_password_invalid' => 8000,
    'user_password_invalid' => 8001,
    'user_priority' => 8002,
    'account_deactive' => 8003,
    'users_available' => 8004, //when send request create user
    'fieldSort_not_found' => 8011,
    'fieldSearch_not_found' => 8012,
);

$ApiCodes['ApiErrorMessages'] = $ApiErrorMessages;
$ApiCodes['ApiErrorCodes'] = $ApiErrorCodes;

return $ApiCodes;
