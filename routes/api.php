<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route Authentication
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('request/reset-password', 'AuthController@requestResetPassword');
    Route::post('accept/reset-password', 'AuthController@acceptResetPassword');
    Route::group(['middleware' => ['jwt']], function () {
        Route::put('change-password', 'AuthController@changePassword');
        Route::post('logout', 'AuthController@logout');
    });
});

//Route user
Route::group(['prefix' => 'user'], function () {

    Route::post('accept/create-user', 'AdminController@acceptCreateUser');
    Route::middleware(['jwt'])->group(function () {
        Route::get('all-activities', 'UserController@getAllActivities');
        Route::get('reactions-activities', 'UserController@getReactionsActivities');
        Route::get('comments-activities', 'UserController@getCommentsActivities');

        Route::get('profile', 'UserController@getProfile');
        Route::post('change-avatar', 'UserController@changeUserAvatar');
        Route::get('search', 'UserController@search');
    });
});

//Route admin -- only Admin can access
Route::group(['prefix' => 'admin'], function () {
    //manage user
    Route::post('accept/create-user', 'AdminController@acceptCreateUser');

    Route::middleware(['jwt', 'admin'])->group(function () {
        Route::post('request/create-user', 'AdminController@requestCreateUser');
        Route::get('all-user', 'AdminController@getAllUser');
        Route::put('/{id}', 'AdminController@editUser');
        Route::delete('/{id}', 'AdminController@deleteUser');
    });
});

//Route product
Route::group(['prefix' => 'product'], function () {
    Route::group(['middleware' => ['jwt']], function () {
        Route::get('/{id}', 'ProductController@getProduct');

        // Route::get('/get/all?page={page}', 'ProductController@getAllProducts'); //add query to filter products
        Route::get('/get/all', 'ProductController@getProductsOfCategory'); //add query to filter products

        Route::post('/create', 'ProductController@createProduct');
        Route::post('/update/{product_id}', 'ProductController@updateProduct');
    });
});


//Route product comment
Route::group(['prefix' => 'product-comment'], function () {
    Route::group(['middleware' => ['jwt']], function () {
        Route::get('/{product_id}', 'ProductCommentController@getProductComment');
        Route::post('/post', 'ProductCommentController@postProductComment');
        Route::put('/put', 'ProductCommentController@putProductComment');
        Route::delete('/delete', 'ProductCommentController@deleteProductComment');
    });
});

//Route Carousel
Route::group(['prefix' => 'carousel'], function () {
    Route::get('/get-all', 'CarouselController@get');
    Route::post('/create', 'CarouselController@createCarousel');
});

//Route category
Route::group(['prefix' => 'category'], function () {
    Route::group(['middleware' => ['jwt']], function () {
        Route::get('/all', 'CategoryController@getAllCategory'); //add query to filter categories
        Route::get('/{id}', 'CategoryController@getInforOfCategory');

        Route::post('/create', 'CategoryController@createCategory');
        Route::post('/update/{cat_id}', 'CategoryController@updateCategory');
    });
});

//Email collection
Route::post('/new-email-collection', 'CollectEmailController@newEmailCollection');

//Stock
Route::group(['prefix' => 'stock'], function () {
    Route::group(['middleware' => ['jwt', 'admin']], function () {
        Route::post('/import', 'StockController@importStock');
        Route::post('/export', 'StockController@exportStock');
    });
});

//cart
Route::group(['prefix' => 'cart'], function () {
    Route::group(['middleware' => ['jwt']], function () {

        Route::get('/view/{cart_id}', 'CartController@viewCart');

        Route::post('/add', 'CartController@addToCart');
        Route::post('/subtract', 'CartController@subtractFromCart');
        Route::delete('/remove', 'CartController@removeProductFromCart');
    });
});

//checkout
Route::group(['prefix' => 'checkout'], function () {
    Route::group(['middleware' => ['jwt']], function () {
        Route::get('/bill', 'CheckOutController@viewBill');

        Route::middleware(['admin'])->group(function () {
            Route::post('/finished', 'CheckOutController@finishedShip');
            Route::delete('/cancel', 'CheckOutController@cancelShip');
        });

        Route::post('/user-cart', 'CheckOutController@checkout');
    });
});
